# cloudron-linkding
Package to deploy linkding to Cloudron


To build the image you need to clone (or unzip) the [linkding source](https://github.com/sissbruecker/linkding) to the repo folder and name it linkding

after deployment to Cloudron you have to setup a super user in terminal:
```
cd /app/data/
python3 manage.py createsuperuser --username=john --email=john@doe.com
```
