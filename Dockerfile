FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

ARG VERSION=1.19.0

RUN mkdir -p /app/

RUN cd /tmp && \
    wget https://github.com/sissbruecker/linkding/archive/refs/tags/v${VERSION}.zip && \
    unzip v${VERSION} -d /tmp/ && \
    rm /tmp/v${VERSION}.zip && \
    mv /tmp/linkding-${VERSION} /app/code

RUN ln -s /usr/bin/python3 /usr/bin/python

WORKDIR /app/code
 
RUN mkdir -p /app/code/data/favicons

RUN npm install -g npm && npm install
RUN npm run build
RUN pip install -U pip && pip install -Ur requirements.txt

RUN python manage.py compilescss && \
    python manage.py collectstatic --ignore=*.scss && \
    python manage.py compilescss --delete-files

RUN pip install uWSGI

# Expose uwsgi server at port 9090
EXPOSE 9090
COPY uwsgi.ini .
COPY start.sh .
# Allow running containers as an an arbitrary user in the root group, to support deployment scenarios like OpenShift, Podman
RUN ["chmod", "g+w", "."]

# Run bootstrap logic
CMD ["/app/code/start.sh"]
